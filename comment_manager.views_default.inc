<?php
/**
 * Implementation of hook_views_default_views().
 */
function comment_manager_views_default_views() {
  $view = new view;
  $view->name = 'admin_comments';
  $view->description = 'View, edit and delete your site\'s comments.';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'comments';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $relationships = array(
    'nid' => array(
      'label' => 'Node',
      'required' => 0,
      'id' => 'nid',
      'table' => 'comments',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'uid' => array(
      'label' => 'User',
      'required' => 0,
      'id' => 'uid',
      'table' => 'comments',
      'field' => 'uid',
      'relationship' => 'none',
    ),
  );
  if (module_exists('votingapi')) {
    $relationships += array(
    'votingapi_cache' => array(
      'label' => 'Voting results',
      'required' => 0,
      'votingapi' => array(
        'value_type' => 'points',
        'tag' => 'vote',
        'function' => 'count',
      ),
      'id' => 'votingapi_cache',
      'table' => 'comments',
      'field' => 'votingapi_cache',
      'relationship' => 'none',
    ),
    );
  }
  $handler->override_option('relationships', $relationships);

  $fields = array(
    'title' => array(
      'label' => 'Story Title',
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'nid',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'name_1' => array(
      'label' => 'Author',
      'link_to_user' => 1,
      'exclude' => 0,
      'id' => 'name_1',
      'table' => 'comments',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'comment' => array(
      'label' => 'Comment',
      'exclude' => 0,
      'id' => 'comment',
      'table' => 'comments',
      'field' => 'comment',
      'relationship' => 'none',
    ),
    'timestamp_1' => array(
      'label' => 'Post date',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'timestamp_1',
      'table' => 'comments',
      'field' => 'timestamp',
      'relationship' => 'none',
    ),
    'access' => array(
      'label' => 'Last access',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'access',
      'table' => 'users',
      'field' => 'access',
      'relationship' => 'nid',
    ),
  );  
  if (module_exists('votingapi')) {
    $fields += array(
    'value' => array(
      'label' => 'Vote',
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'appearance' => NULL,
      'exclude' => 0,
      'id' => 'value',
      'table' => 'votingapi_cache',
      'field' => 'value',
      'relationship' => 'votingapi_cache',
    ),
    );
  }
  $handler->override_option('fields', $fields);
  $handler->override_option('filters', array(
    'title' => array(
      'operator' => 'contains',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'title_op',
        'identifier' => 'title',
        'label' => 'Story Title',
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'nid',
    ),
    'comment' => array(
      'operator' => 'contains',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'comment_op',
        'identifier' => 'comment',
        'label' => 'Comment keywords',
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 0,
      'id' => 'comment',
      'table' => 'comments',
      'field' => 'comment',
      'relationship' => 'none',
    ),
    'uid' => array(
      'operator' => 'in',
      'value' => array(
        '0' => 0,
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'uid_op',
        'identifier' => 'uid',
        'label' => 'Comment author',
        'optional' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'relationship' => 'uid',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'administer nodes',
  ));
  $handler->override_option('title', 'Manage stories');
  $handler->override_option('empty', 'There are no objects satisfying the filter settings. Try changing them to get some results.');
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 25);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'bulk');
  $info = array(
      'title' => array(
        'sortable' => 1,
        'separator' => '&nbsp;',
      ),
      'name_1' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'comment' => array(
        'separator' => '',
      ),
      'timestamp_1' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'access' => array(
        'sortable' => 1,
        'separator' => '',
      ),
  );  
  if (module_exists('votingapi')) {
    $info += array(
      'value' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    );
  }
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'desc',
    'columns' => array(
      'title' => 'title',
      'name_1' => 'name_1',
      'value' => 'value',
      'comment' => 'comment',
      'timestamp_1' => 'timestamp_1',
      'access' => 'access',
    ),
    'info' => $info,
    'default' => 'timestamp_1',
    'execution_type' => '2',
    'display_type' => '0',
    'skip_confirmation' => 1,
    'display_result' => 1,
    'merge_single_action' => 1,
    'selected_operations' => array(
      '90e3275a4af1fdf68f7ce9621a3a7f14' => '90e3275a4af1fdf68f7ce9621a3a7f14',
      'b1500c08f3b29d92a795c5e60736b613' => 'b1500c08f3b29d92a795c5e60736b613',
      '2178a36c0b51f3a7ea1d854780e33cc5' => '2178a36c0b51f3a7ea1d854780e33cc5',
      '2a803d8560883cff011070ff5aae2908' => '2a803d8560883cff011070ff5aae2908',
      '350c75db8017d54bfe1b3e8e2ee48408' => '350c75db8017d54bfe1b3e8e2ee48408',
      '7db0a7095ba86404d0e3de01a95bd10e' => '7db0a7095ba86404d0e3de01a95bd10e',
      '7c8302f5507d7f96c2a159e69455e936' => '7c8302f5507d7f96c2a159e69455e936',
      '132fddcb3f367a243bee632db31985c1' => 0,
      'daa75f478e3093ab107e657da6620a91' => 0,
      '52aec7fee2070ce530da1304653ae1ec' => 0,
      'feb13e750bd2575b1f36109233087905' => 0,
      '246fdc2a4672eb371d05b48b2a7cb51e' => 0,
      '334d20af1ae7ac4b770b7ec3210b2638' => 0,
    ),
  ));
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->override_option('path', 'admin/content/manage/comments');
  $handler->override_option('menu', array(
    'type' => 'Normal',
    'title' => 'Manage comments',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $views[$view->name] = $view;

  return $views;
}
